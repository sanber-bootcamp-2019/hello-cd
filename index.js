const http = require("http");

const server = http.createServer((req, res) => {
  res.write('hai-aku-sudah-berubah-jadi-fitur-a');
  res.end();
});

const PORT = process.env['PORT'] || 80;

server.listen(PORT, () => {
  process.stdout.write('server listen on port ' + PORT);
});
